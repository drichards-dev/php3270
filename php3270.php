<?php
/**
 * @package extensions\php3270
 */

//namespace extensions;



/**
 * Class to handle 3270 terminals.
 *
 * Class that implements in PHP the execution of commands and
 * reading information on 3270 terminal screens.
 * It is implemented using the s3270 program (Unix/Linux), being
 * so the same is required for the use of this class.
 * @see http://x3270.bgp.nu/s3270-man.html http://x3270.bgp.nu/s3270-man.html
 *
 * @author Murilo Klock Ferreira <murilo.klock@gmail.com>
 * Comments translated to English by Duane Richards and Google Translate
 *
 * @version 0.1
 */
class php3270 {

   /**
    * vector with the definition of the read and write pointers
    * @var array vector with definition of read and write pointers
    */
   var $descriptorspec = array(
      0 => array("pipe","r"),
      1 => array("pipe","w"),
      2 => array("pipe","w")
   );

   /**
    * vector with read and write pointers
    * @var array vector with read and write pointers
    */
   var $pipes=array();

   /**
    * Variable to save the resource obtained by opening the s3270
    * @var resource resource for s3270
    */
   var $process="";

   /**
    * Variable to store the screen with data
    * @var array stores the data read from the 3270 terminal screen in an array
    */
   var $screen ="";

   /**
    * Variable to store the number of lines of the 3270 terminal
    * @var int number of lines from terminal 3270
    */
   var $lines="";

   /**
    * Variable to store the number of columns of the 3270 terminal
    * @var int number of columns of terminal 3270
    */
   var $columns="";

   /**
    * Variable to save the error text of the 3270 terminal
    * @var string error description on 3270 terminal
    */
   var $error="";

   /**
    * Variable to store 3270 terminal status data
    * @var array vector the status data of the 3270 terminal
    */
   var $status_msg=array();

   /**
    * Variable to turn the debug on or off
    * @var boolean turns debugging on or off
    */
   var $debug=false;

   /**
    * Variable to save connection status, connected or disconnected.
    * @var boolean connected or disconnected
    */
   var $connected=true;

   /**
    * Variable to save the waiting time when needed.
    * Default is 0, no connection-expectation, connected or disconnected.
    * @var int time in microseconds
    */
   var $waittime=0;

   /**
    * Class Builder. It opens a process enabling reading and writing to it.
    * Forces writing and reading pointers to non-locking mode.
    * If the process is successfully opened, it opens a connection with the server.
    * @see s3270::connect function to open server connection.
    *
    * @param string $hostname Server name or IP address
    * @param int $port port to be used to connect
    *
    * @return boolean
    */
   function __construct($hostname, $port=23) {
      $this->process = proc_open('/usr/bin/s3270', $this->descriptorspec, $this->pipes, null, null);
      stream_set_blocking($this->pipes[0], 0);
      stream_set_blocking($this->pipes[1], 0);
      #$this->debug=true;

      if(is_resource($this->process)) {
         $this->connect($hostname, $port);
         return true;
      }
      else {
         $this->error="resource error\n";
         echo $this->error;
         return false;
      }
   }


   /**
    * Send clear command to server
    *
    * @return void
    */
   final public function clear() {
      if($this->debug) echo "Job: ".__FUNCTION__."\n";

      $this->sendCommand("Clear()");
   }


   /**
    * Open the connection to the server within the s3270 process.
    *
    * @param string $hostname Server name or IP address
    * @param int $port port to be used to connect
    *
    * @return boolean
    */
   private function connect($hostname, $port=23) {

      fwrite($this->pipes[0], "connect(".$hostname.":".$port.")\n");

      $this->waitUnlock();
      $this->waitInputField();
      $this->waitReturn();
      //fwrite($this->pipes[0], "enter()\n");
      //$this->waitReturn();
   }


   /**
    * Ends connection to 3270 server
    *
    * @return void
    */
   function disconnect() {

      #$this->sendCommand('PF(10)');
      #$this->waitReturn();

      #$this->sendCommand('PF(12)');
      #$this->waitReturn();

      #@fwrite($this->pipes[0], "Disconnect()\n");
      $this->sendCommand("Disconnect()");

      $retorno="";
      $tela=array();


      //$this->waitReturn();
   }

   /**
    * Ascii(row,col,rows,cols)
    * or
    * Ascii(row,col,length)
    * or
    * Ascii(length)
    *
    * Displays an ASCII text representation of the screen.
    * Each line is preceded by the text "data: ", and there are no characters from
    * control.
    *
    * If four arguments are given, a rectangular region
    * from the screen is the output.
    *
    * If three arguments are given, $length characters are output,
    * starting with the specified row and column.
    *
    * If only the length argument is given, this number of characters is
    * returned as output, starting at the current cursor position.
    *
    * If no arguments are given, the entire screen is output.
    */
   function ascii() {
      if($this->debug) echo "Job: ".__FUNCTION__."\n";

      $argumentos = func_num_args();
      $lista      = func_get_args();

      if($argumentos > 4) die("Fatal, number of arguments for ascii greater than 4.");

      foreach($lista as $a=>$b) $lista[$a]=$b--;
      #die("Lista:#".implode(',',$lista)."#");
      $this->sendCommand("Ascii(".implode(',',$lista).")");
      #return $this->rectangle( implode(',',$lista) );
      #fwrite($this->pipes[0], "Ascii(".implode(',',$lista).")\n");
   }


   /**
    * Send a command to the 3270 terminal
    *
    * @param string $cmd command to be sent
    * @link http://x3270.bgp.nu/s3270-man.html#Actions http://x3270.bgp.nu/s3270-man.html#Actions
    *
    * @return boolean
    */
   final public function sendCommand($cmd) {
      if($this->debug) echo "Job: ".__FUNCTION__."\n";

      if(is_resource($this->process)) {
         if($this->debug) echo " writing ".'fwrite($this->pipes[0], '.$cmd.')\n");'."<br />\n";
         fwrite($this->pipes[0], $cmd."\n");
         #fwrite($this->pipes[0], "ascii()\n");

         //$this->waitUnlock();

         #$this->esperaCampo();
         #$this->waitReturn();

      }
      else {
         $this->error="resource error\n";
         echo $this->error;
         return false;
      }

      return true;
   }


   /**
    * Send a text to the 3270 terminal
    *
    * @param string $texto command to be sent
    * @param int [$row] optional line where the text will be placed. The first line is 1.
    * @param int [$col] optional column where the text will be placed. The first column is 1.
    * @link http://x3270.bgp.nu/s3270-man.html#Actions http://x3270.bgp.nu/s3270-man.html#Actions
    *
    * @return boolean
    */
   final public function sendString($string,$row=false,$col=false) {
      if($this->debug) echo "Job: ".__FUNCTION__."\n";

      if( $row!=false and $col!=false ) {
         $this->moveCursor($row,$col);
      }

      if(is_resource($this->process)) {
         if($this->debug) echo " writing ".'fwrite($this->pipes[0], "string("'.$string.'")\n");'."<br />\n";
         fwrite($this->pipes[0], "string(".$string.")\n");
      }
      else {
         $this->error="resource error\n";
         echo $this->error;
         return false;
      }

      return true;
   }


   /**
    * Wait for an information input field to appear in the read buffer
    *
    * @return boolean
    */
   final public function waitInputField() {
      if($this->debug) echo "Job: ".__FUNCTION__."\n";

      if(is_resource($this->process)) {
         fwrite($this->pipes[0], "Wait(InputField)\n");
      }
      else {
         $this->error="resource error\n";
         echo $this->error;
         return false;
      }

      return true;
   }



   /**
    * Wait until keyboard is unlocked
    *
    * @return boolean
    */
   final public function waitUnlock() {
      if($this->debug) echo "Job: ".__FUNCTION__."\n";

      if(is_resource($this->process)) {
         fwrite($this->pipes[0], "Wait(Unlock)\n");
         #fwrite($this->pipes[0], "enter\n");
         #fwrite($this->pipes[0], "ascii()\n");
         #$this->readScreen();
      }
      else {
         $this->error="resource error\n";
         echo $this->error;
         return false;
      }

      return true;
   }

   /**
    * Wait until there is data in the read buffer
    *
    * @return boolean
    */
   final public function waitOutput() {
      if($this->debug) echo "Job: ".__FUNCTION__."\n";

      if(is_resource($this->process)) {
         fwrite($this->pipes[0], "Wait(Output)\n");
         #fwrite($this->pipes[0], "enter\n");
         #fwrite($this->pipes[0], "ascii()\n");
         #$this->readScreen();
      }
      else {
         $this->error="resource error\n";
         echo $this->error;
         return false;
      }

      return true;
   }


   /**
    * Wait until the number of data lines defined in the 3270 application
    * are received
    *
    *
    * @param int $lines number of lines. 24 lines pattern
    *
    * @return boolean
    */
   final public function waitReturn($rows=false) {
      if($this->debug) echo "Job: ".__FUNCTION__."\n";
      $this->status_msg=array();

      if(is_resource($this->process)) {
         while(true) {

            $return = fgets($this->pipes[1]);
            if( $this->debug===true ) echo "\n\n".$return."\n\n";
            $re='/([U|L|E])\s([F|U])\s([P|U])\s(C\(.*\)|N)\s([I|L|C|P|N])\s([2-5])\s([0-9]+)\s([0-9]+)\s([0-9]+)\s([0-9]+)\s(0x[0-9]+)\s([0-9\.\-]+)/';

            if( preg_match($re,$return,$tmp) ) {
               if( $this->debug===true ) echo "\n\n".$return."\n\n";
               if( isset($tmp[1]) ) $this->status_msg['keyboard_state']   =$tmp[1];
               if( isset($tmp[2]) ) $this->status_msg['screen_formatting']=$tmp[2];
               if( isset($tmp[3]) ) $this->status_msg['field_protection'] =$tmp[3];
               if( isset($tmp[4]) ) $this->status_msg['connection_state'] =$tmp[4];
               if( isset($tmp[5]) ) $this->status_msg['emulator_mode']    =$tmp[5];
               if( isset($tmp[6]) ) $this->status_msg['model_number']     =$tmp[6];
               if( isset($tmp[7]) ) $this->status_msg['number_of_rows']   =$tmp[7];
               if( isset($tmp[8]) ) $this->status_msg['number_of_cols']   =$tmp[8];
               if( isset($tmp[9]) ) $this->status_msg['cursor_row']       =$tmp[9];
               if( isset($tmp[10]) ) $this->status_msg['cursor_col']      =$tmp[10];
               if( isset($tmp[11]) ) $this->status_msg['window_id']       =$tmp[11];
               if( isset($tmp[12]) ) $this->status_msg['command_exec_time']=$tmp[12];
            }
            #if( $this->debug===true ) print_r( $this->status_msg);
            if(!$rows and !empty($this->status_msg['number_of_rows'])) $rows=$this->status_msg['number_of_rows'];
            elseif(!$rows) $rows=24;
            if( isset($this->status_msg['keyboard_state'])
                and $this->status_msg['keyboard_state']=='E' ) $this->clear();

            if( preg_match( '/ok/',$return) ) {
               #echo "\n\n".$return."\n\n";
               #if( $this->debug===true ) print_r( $this->status_msg);
               $this->readScreen($rows);
               return true;
            }
            elseif( preg_match( '/error/',$return) ) {
               $this->readScreen($rows);
               $this->error="Error connecting to MainFrame server\n";
               $this->connected=false;
               return false;
            }
            elseif( preg_match( '/locked/is',$return) ) {
               $this->reset();
            }
            elseif( preg_match( '/clean/is',$return) ) {
               $this->clear();
            }
            elseif( preg_match( '/wait/is',$return) ) {
               $this->waitUnlock();
            }
         }
      }
      else {
         $this->error="resource error\n";
         echo $this->error;
         return false;
      }
   }


   /**
    * Sends an action called Program Function
    * which can be a number between 1 and 24 (PF(1) to PF(24))
    *
    *
    * @param int $n share number (between 1 and 24).
    *
    * @return boolean
    */
   final public function pf($n) {
      if($this->debug) echo "Job: ".__FUNCTION__."\n";

      if( !is_numeric($n) and ($n<1 or $n>24) ) {
         $this->error="Program job must be a number between 1 and 24.";
         return false;
      }

      $this->sendCommand("PF(".$n.")");
   }



   /**
    * Reads the defined number of data lines defined in the 3270 application
    *
    *
    * @param int $lines number of lines. 24 lines pattern
    *
    * @return boolean
    */
   final public function readScreen($rows=false) {
      if($this->debug) echo "Job: ".__FUNCTION__."\n";

      $this->screen =  array();
      if(!$rows) $rows=$this->status_msg['number_of_rows'];
      $inicio = false;
      $retorno = "";

      if(is_resource($this->process)) {
         #fwrite($this->pipes[0], "Wait(Output)\n");
         #fwrite($this->pipes[0], "ascii()\n");
         $this->waitOutput();
         $this->ascii();
         if( $this->debug===true ) {
            echo 'Number of lines in $this->screen: '.count($this->screen)."\n";
            echo 'Number of lines in $rows: '.$rows."\n";
         }
         while ( count($this->screen) < $rows) {
            $retorno = fgets($this->pipes[1]);

            if( preg_match( '/locked/is',$retorno) ) {
               $this->reset();
            }
            elseif( preg_match( '/clean/is',$retorno) ) {
               $this->clear();
            }
            elseif( preg_match( '/wait/is',$retorno) ) {
               $this->waitUnlock();
            }

            #if( $this->debug===true ) {
            #   echo "Retorno: ".$retorno."\n";
            #}
            #if( count($this->screen)>1 and preg_match('/^data:(.*)/i',$retorno,$tmp) and !$inicio ) {
            #   if( trim($tmp[1]) == '' ) continue;
            #}
            #else $inicio=false;

            if( !empty($retorno) and preg_match('/^data/',$retorno) ) $this->screen[] = $retorno;
            #fwrite($pipes[0], "ascii()\n");
            usleep($this->waittime);
         }
         if( $this->debug===true ) {
            echo "Let's print the vector with the return\n";
            print_r($this->screen);
         }
         return true;
      }
      else {
         $this->error="resource error\n";
         echo $this->error;
         return false;
      }
   }

   /**
    * rectangle(row,col,rows,cols)
    * or
    * rectangle(row,col,length)
    * or
    * rectangle(length)
    *
    * Displays an ASCII text representation of the screen.
    * Each line is preceded by the text "data: ", and there are no characters from
    * control.
    *
    * If four arguments are given, a rectangular region
    * from the screen is the output.
    *
    * If three arguments are given, length characters are output,
    * starting with the specified row and column.
    *
    * If only the length argument is given, this number of characters is
    * returned as output, starting at the current cursor position.
    *
    * If no arguments are given, the entire screen is output.
    */
   final public function rectangle() {
      if($this->debug) echo "Job: ".__FUNCTION__."\n";

      $argumentos = func_num_args();
      $lista      = func_get_args();
      $saida      = '';

      if($argumentos > 4) die("Fatal, number of arguments for ascii greater than 4.");

      if( $argumentos== 4 ) {
         $lista[0]=($lista[0]-1);
         $lista[2]=($lista[2]-1);
      }
      if( $argumentos== 3 ) $lista[0]=$lista[0]-1;

      $tmp = $this->screen;
      foreach($tmp as $a => $b) $tmp[$a]=preg_replace('/^data:/','',$b);


      if($argumentos==4) {
         foreach($tmp as $a=>$b) {
            if($a<$lista[0] or $a>$lista[2]) continue;
            $saida.=substr($b,$lista[1],($lista[3]-$lista[1]))."\n";
         }
      }

      if($argumentos==3) {
         $saida=substr($tmp[$lista[0]],$lista[1],$lista[2]);
      }
      elseif($argumentos==1 and !empty($lista[0])) {
         $saida=substr($b,0,$lista[0]);
      }
      elseif($argumentos==0) {
            $saida=implode("\n",$tmp);
      }
      return $saida;
   }


   /**
    * Send reset command to server
    *
    * @return void
    */
   final public function reset() {
      if($this->debug) echo "Job: ".__FUNCTION__."\n";

      $this->sendCommand("Reset()");
   }

   /**
    * Sends EraseInput command to server. Clear all values in fields
    * data entry
    *
    * @return void
    */
   final public function eraseInput() {
      if($this->debug) echo "Job: ".__FUNCTION__."\n";

      $this->sendCommand("EraseInput()");
   }

   /**
    * Move cursor to position determined by row and column entered
    * @param int $row
    * @param int $col
    * @return boolean
    */
   final public function moveCursor($row,$col) {
      if($this->debug) echo "Job: ".__FUNCTION__."\n";

      if( $row!=false and $col!=false ) {
         $row--;
         $col--;
      }
      $this->sendCommand("MoveCursor(".$row.", ".$col.")");
      #fwrite($this->pipes[0], "MoveCursor(".$row.", ".$col.")\n");
      #$this->waitUnlock();
      return true;
   }



   /**
    * Converts data read from Array format to String
    *
    *
    * @return string $strTela 3270 terminal screen data in text format.
    */
   final public function screenToString() {
      if($this->debug) echo "Job: ".__FUNCTION__."\n";

      $strTela='';
      if( isset($this->screen) and is_array($this->screen) ) {
         $tmp = $this->screen;
         foreach($tmp as $a => $b) $tmp[$a]=preg_replace('/^data:/','',$b);
         $strTela = implode( "", $tmp );
      }
      return $strTela;
   }


   /**
    * Job to wait for certain text to appear on the screen (buffer)
    *
    * @param string string to be expected
    *
    * @return string $screen screen text
    */
   final public function waitString ($string) {
      if($this->debug) echo "Job: ".__FUNCTION__."\n";

      $x = false;
      $g = false;
      $tela = "";
      $linha=1;


      while ($x == false) {
         usleep(1000);
         while (false !== ($mydat = fgets($this->pipes[1]))) {
            echo $mydat."\n";
            $tela.=$mydat;
            if (preg_match("/".$string."/i", $mydat)) {
               $g = true;
            }
            if ($g == true) {

               while (false !== ($mydat = fgets($this->pipes[1]))) {

                  $tela.=$mydat;
                  #echo $mydat."\n";
               }

               $x = true;
               break 2;
            }
         }
      #fwrite($pipes[0], "ascii()\n");
      }
      return $tela;
   }



   /**
    * Class destroyer. Closes open read and write pointers
    * and closes the process.
    */
   function __destruct() {

      foreach ($this->pipes as $pipe) {
         @fclose($pipe);
      }

      @proc_close($this->process);
   }


}

?>
